// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB8Is6Qk5dynAalnJZMy0DhaheSVQzwjqQ",
    authDomain: "veterinaria-tcc.firebaseapp.com",
    databaseURL: "https://veterinaria-tcc.firebaseio.com",
    projectId: "veterinaria-tcc",
    storageBucket: "veterinaria-tcc.appspot.com",
    messagingSenderId: "1036103578214",
    appId: "1:1036103578214:web:e6080e065e053188bc73a7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
