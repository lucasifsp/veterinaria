import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Animal } from '../interfaces/animal';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  private animalsCollection: AngularFirestoreCollection<Animal>;

  constructor(private afs: AngularFirestore) {
    this.animalsCollection = this.afs.collection<Animal>('Animals');
  }

  getAnimals(){
    return this.animalsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        })
      })
    )
  }

  addAnimal(animal: Animal){
    return this.animalsCollection.add(animal);
  }

  getAnimal(id: string){
    return this.animalsCollection.doc<Animal>(id).valueChanges();
  }

  updateAnimal(id: string, animal: Animal){
    return this.animalsCollection.doc<Animal>(id).update(animal);
  }

  deleteAnimal(id: string){
    return this.animalsCollection.doc(id).delete();
  }
}
