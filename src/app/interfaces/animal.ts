export interface Animal {
    id?: string;
    name?: string;
    tutorName?: string;
    picture?: string;
    breed?: string;
    address?: string;
    note?: string;
    createdAt?: number;
    userId?: string;
}
