import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/app/interfaces/animal';
import { Subscription } from 'rxjs';
import { AnimalService } from 'src/app/services/animal.service';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public animals = new Array<Animal>();
  public animalsSubscription: Subscription;
  private loading: any;

  constructor(
    private animalsService: AnimalService,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
    ) { 
    this.animalsSubscription = this.animalsService.getAnimals().subscribe(data => {
      this.animals = data;
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.animalsSubscription.unsubscribe();
  }

  async logout(){
    try {
      await this.authService.logout();
    } catch(error) {
      console.error(error);
    }
  }

  async deleteAnimal(id: string){
    try {
      await this.animalsService.deleteAnimal(id);
    } catch (error) {
      this.presentToast('Erro ao tentar excluir!');
    }
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({message: 'Por favor, aguarde...'});
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({message, duration: 2000});
    toast.present();
  }
}
